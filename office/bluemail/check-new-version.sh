#!/bin/bash
VERSION=`curl -s -H 'X-Ubuntu-Series: 16' https://api.snapcraft.io/api/v1/snaps/details/bluemail | jq '.version' -r`
DOWNLOAD=`curl -s -H 'X-Ubuntu-Series: 16' https://api.snapcraft.io/api/v1/snaps/details/bluemail | jq '.download_url' -r`
echo "current version: $VERSION"
echo "download-link:   $DOWNLOAD"
exit 0
